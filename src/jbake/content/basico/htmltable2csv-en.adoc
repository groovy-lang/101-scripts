= Html Table to CSV
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2018-06-24
ifndef::backend-pdf[]
:username: jorge-aguilera
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, html, csv
:jbake-category: basico
:jbake-script: /scripts/basico/HtmlTable2Csv.groovy
:idprefix:
:imagesdir: ../images
:jbake-lang: gb
:jbake-spanish: htmltable2csv
endif::[]

In this simple script we'll see how to parse an HTML page with a table (in our sample with and `id`
but it's easy to contemplate other use cases) and how to dump it to a `csv` file.

[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----

This script needs 3 arguments:

- the URL to parse
- the filename to use
- the id of the table

Once the script has downloaded and parsed the page it'll search an element with the `id` equals to the
argument and it'll iterate across all `tr` elements generating one line per each.

It'll join all `td` elements for each `tr` using `;` as field separator





include::{contentdir}/include-source.txt[]
