= CliBuilder
Miguel Rueda <miguel.rueda.garcia@gmail.com>
2017-08-30
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: basico
:jbake-script: /scripts/basico/CliBuilder.groovy
:idprefix:
:imagesdir: ../images
:jbake-english: clibuilder-en
endif::[]

Se puede dar la casuística de tener un script que realiza unas determinadas funciones que queremos parametrizar.

La opción más fácil es usar la variable *args* ímplicita en el script y que es un array de String que contiene
los parámetros que se pasan tras el nombre del fichero.

Sin embargo cuentas también con `CliBuilder`, una utilidad de Groovy que te permite hacer que los argumentos que se le pueden
pasar a un script sean más explícitos.

Un script muy básico utilizando esta herramienta podría ser el siguiente:

[source,ruby]
----
include::{sourcedir}{jbake-script}[]
----
<1> Definimos los parámetros que vamos a necesitar.
<2> Mostrará la leyenda de nuestro comando.
<3> Esta parte se ejecutará al mandar como parametro `-a`.

Si por ejemplo llamamos a nuestro script pasando el parámtro `-h` obtendremos la leyenda de nuestro comando:

----
groovy clibuilder_ebook.groovy  -h
usage: clibuilder_ebook.groovy -[habcd]
 -a,--Option a   Al seleccionar "a" pinta seleccionada -> a
 -b,--Option b   Al seleccionar "b" pinta seleccionada -> b
 -c,--Option c   Al seleccionar "c" pinta seleccionada -> c
 -d,--Option d   Al seleccionar "d" pinta seleccionada -> d
 -h,--help       Usage Information
----



include::{contentdir}/include-source.txt[]
