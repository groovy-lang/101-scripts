= Importar datos de un excel a una tabla
Miguel Rueda <miguel.rueda.garcia@gmail.com>
2017-10-08
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: office
:jbake-script: /scripts/office/ExcelToBBDD.groovy
:idprefix:
:imagesdir: ../images
endif::[]



Seguramente a lo largo de tu vida te habrás encontrado con algún cliente/compañero/amigo/no tan amigo al cual le has
preguntado dónde guarda una determinada información sobre la que estáis trabajando y  te ha contestado que en un Excel
en su local. Tras la risa nerviosa habrás pasado por la irá, hasta acabar en la etapa del miedo. Bueno una vez la calma
llegue podrás darle la opción de incluirlo en la base de datos con la que trabajáis. Para ello pasamos a explicar cómo
realizarlo.

Para trabajar con este script vamos a necesitar las siguientes librerías:
[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=grab]
----
El siguiente paso que vamos a realizar será la conexión con la base de datos, en este caso contra nuestra máquina.
[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=mysql]
----
Las variables necesarias para este script las hemos definido dentro del mismo pero una forma mejor de hacerlo sería
recibir estar información por parámetro y comprobando que cada uno de los datos son correctos, es decir, que el fichero
existe, que la conexión con la base de datos es correcta y por último que existe la tabla en la que queremos cargar los
datos. En este caso práctico nos vamos a centrar únicamente en la carga de datos por lo tanto vamos a dar valor a cada
uno de los valores necesarios:
[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=variables]
----

Una vez tenemos los requisitos necesarios vamos a crear el método `parse` al cual le pasaremos por parámetro la ruta
donde se encuentra nuestro Excel y él creará un array con los datos de nuestro fichero, en este caso un nombre fijado a `myExcel.xlsx`

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=parse]
----
<1> Con el objeto `WorkbookFactory`, accediendo al método create podemos acceder a nuestro fichero y con el método
`getSheetAt(0)`  del objeto `Workbook` podremos obtener la primera hoja (Sheet) de nuestro Excel.

<2> Para recorrer nuestra hoja seleccionada utilizaremos la función `rowIterator()` que nos va permitir acceder a los
valores que se encuentran en cada una de las filas.

<3> La primera fila asumimos que es la cabecera del Excel y que contiene el nombre descriptivo de cada columna. Para
obtener este mapa utilizamos nuestro método `getRowData`:
[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=row]
----
El cual recorre cada una las filas que tiene nuestro Excel convirtiendo cada valor en información con la que podamos
trabajar, dependiendo del formato que se encuentre en cada celda, es decir, si en una cadena lo formaterá a String, si
es un valor entero a int...

Para este trabajo contamos con el método `getValue`:
[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=value]
----

En el cual podemos ver claramente como formatea cada unos de los valores. Como lo realizar por ejemplo si el formato de
la celda es un String:
[source,groovy]

----
include::{sourcedir}{jbake-script}[tags=string_f]
----

Bien ya hemos conseguido leer el nuestro fichero y convertirlo en una lista con la que podemos trabajar. Nuestro siguiente
paso será abordar nuestra tabla en este caso sobre una base de datos mysql. Para ello vamos a utilizar:
[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=insert]
----
<1> Obtenemos el esquema de la tabla recibida.
<2> A través de la herramienta `withBatch` vamos a recorrer la lista de valores e insertar de 20 en 20 en nuestra base de
datos.


include::{contentdir}/include-source.txt[]
