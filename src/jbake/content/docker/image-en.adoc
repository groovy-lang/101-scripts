= Dockerize your own image
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-10-14
ifndef::backend-pdf[]
:username: jorge-aguilera
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: docker
:jbake-script: /scripts/docker/WatchFile.groovy
:idprefix:
:imagesdir: ../images
:jbake-lang: gb
:jbake-spanish: image
endif::[]


In a previous post (*Docker and Groovy*) we reviewed how can we use the official image of Groovy to execute our scripts
into the container where is running. Also with these images we can use all features of Docker as mount volumes,
networking etc and in this way our scripts can works together with others containers.

In this post we'll see how can use these images to build our own image and upload them to our account in Docker Hub, private Nexus,
Google, etc

We'll use a simple script to dump a file to console every 30 seconds in a forever while


.WatchFile.groovy
[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----

INFO:: As you can see in this post the script is not the mos important

== Dockerfile

First of all we need to write an instructions file _Dockerfile_ to send to Docker. The name can be whatever you want
but usually it's _Dockerfile_

[source]
----
FROM groovy:2.4.12-jre8-alpine

COPY WatchFile.groovy /home/groovy/

VOLUME ["/var/watchfile"]

ENTRYPOINT ["groovy", "WatchFile.groovy"]

CMD []
----

We are instructing to Docker who image we can use as base (**groovy:2.4.12-jre8-alpine*), we add a file from our
filesystem and we specify we want to execute a command at the startup of the container ( ``groovy WatchFile.groovy``).

If we run the scripts without parameters it can show us instructions or whatever you want. Or you can specify a defaul
action using *CMD []*

Our image will be mount a volume in */var/watchfile*. In this way our script will be able to read files from others containers or
a subdirectory into the host system and dump it

== Building the image

From the directory where we have both files we execute:

[source,console]
----
docker build --rm -t jagedn/watchfile .  //<1>
----
<1> jagedn/watchfile will be the name of our image. Be aware of the dot at the end

If all works fine you'll have into your system a *jagedn/watchfile* image.

Check it with:

----
docker images | grep jagedn
----

== Run a container

This is an example of how to run a container with this image:

[source,console]
----
docker run --rm -it -v /foo/baar:/var/watchfile jagedn/watchfile /var/watchfile/mifichero.log //<1>
----
<1> /foo/baar is a subdirectory into your machine with a file named _mifichero.log_

Every 30 seconds you will see a dump of the file in the console. You can modify _mifichero.log_ and you'll see in the
next iteraction the changes


As you can see this example is not very interesting but you can see the steps to have your script dockerized.
Rembember to execute the *push* if you want to upload the image to the repository:

----
docker push jagedn/watchfile
----


include::{contentdir}/include-source.txt[]
