= Leer un fichero desde un host remoto
Miguel Rueda <miguel.rueda.garcia@gmail.com>
2017-10-12
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: network
:jbake-script: /scripts/network/SshoogrGetfile.groovy
:idprefix:
:imagesdir: ../images
endif::[]


Seguramente os haya surgido la necesidad de comprobar el contenido de unos determinados equipos remotos
desde vuestro equipo como por ejemplo ver las trazas de un fichero `.log` que va dejando la aplicación que
habéis instalado, comprobar versiones o propiedades guardadas en un fichero, modificar uno existente y querer
comprobar que unos determinados ficheros de configuración apuntan correctamente, etc.

Cuando sólo tienes un equipo que administrar esta tarea no es muy compleja y probablemente la realizas a mano, pero
cuando el número de equipos a comprobar crece la tarea se vuelve muy repetitiva y cansada y es cuando piensas en
crearte un script que realice esta tarea en todas las máquinas por tí.

Para crear el script que facilite esta tarea vamos a emplear https://github.com/aestasit/sshoogr[sshoogr].

Esta herramienta nos permite realizar sobre nuestros equipos muchas más funciones de las que vamos a ver en este post,
como copiar archivos y directorios,
ejecutar comandos remotos ... os ánimo a que reviseis la documentación que seguro que os será de gran ayuda.


A continuación pasamos a explicar en funcionamiento de nuestro script paso a paso:

- Añadimos la dependencia de sshoogr:
+
[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=dependencies]
----

- Creamos la función para leer el fichero:
+
[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=read]
----
<1> Establecemos los parámtros de conexión `usuario = user`, `contraseña = passwd` mientras que el `servidor` es variable.
<2> En la función `remoteFile` le pasamos por parámetro la ruta complete la fichero.
<3> Con la función `readLines` obtenemos una lista con el contenido del fichero.

- Creamos la lista de hosts y el nombre del fichero que queremos leer:
+
[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=config]
----


- recorremos los host y leemos el archivo en cuestion:
+
[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=main]
----
+
NOTE: Si te da error al conectar con el host, prueba a desactivar la comprobación estricta de la clave del host:
+
[source,groovy]
----
options.trustUnknownHosts = true
----




include::{contentdir}/include-source.txt[]
