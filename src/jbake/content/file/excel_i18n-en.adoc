= From Excel to i18n and viceversa
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2018-03-03
ifndef::backend-pdf[]
:username: jorge-aguilera
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc, excel, i18n, file
:jbake-category: file
:jbake-script: /scripts/file/ExcelI18n.groovy
:jbake-spanish: excel_i18n
:jbake-lang: gb
:idprefix:
:imagesdir: ../images
endif::[]

In this days, most Java applications use the standard internalization functionality.
This functionality use different properties files with a common root name plus a suffix who indicate the corresponding locale.

For example, if our application want to show the messages in english,
spanish and french with english as default language we need to have 3 files:


.theapp.properties
[source,java]
----
login=Login
welcome=Welcome
----

.theapp_es.properties
[source,java]
----
login=Identificacion
welcome=Bienvenido
----

.theapp_fr.properties
[source,java]
----
login=Identifier
welcome=Bienvenue
----

As soon the application grows, the number of messages also increased and at the end we concetrate
all our efforts to write all messages in the default language in order to collect all of them and translate later.

Problems come when this series of incompletes files are difficult to edit by the translator becasue there are many files,
he can't copy&paste easily and so on. Most of the time this person will be happy with an Excel file with all
codes and languages in one place

With this script we'll be able to do:

- Take some i18n _properties_ files and collect all codes plus translations into one Excel file organize by codes and
languages

- Take an _excel_ file with the translations and dump into several i18n _properties_ files


NOTE: We'll use Apache POI for both situations but we'll use DSL Groovy Excel Builder from James Kleeh as example of
of easy it's to write a Excel file.

== Dependencies

We'll use Apache POI but as they are declared in the Excel Builder we need to declare only this:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=dependencies]
----

== Argumentes

Our scritp will be able to execute two differents actions so we'll prepare a _CliBuilder_ to parser the command line:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=arguments]
----
<1> prepare options
<2> from Excel to properties
<3> from properties to Excel
<4> a little help

== From i18n to Excel

If in our initial situation we have an incomplete translation we need to import all of them into an Excel wich rows
are the codes and the columns the detected languages (after we can add more languages in the Excel file)

In order to know how many languages we are working on, we need only to use a regular expression to take
_filename_ _? _i18code_? .properties  ( a name, and underscore plus a string optional and a file extension _.properties_)


[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=generateExcel]
----
<1> load into a _properties_ the default file with all keys to translate
<2> prepare a _Map<String,Properties>_
<3> search files who complain the pattern
<4> load every properties and assign a key into the map
<5> dump the map into an Excel using keys as rows and languages as columns


== From Excel to i18n

Once translated the cells we want to dump the Excel file into the corresponding i18n _properties_ files:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=generateProperties]
----
<1> remove older translations
<2> iterate across rows (codes)
<3> first row contains languages to dump (first code it's default language)
<4> for every language we find the corresponding translations
<5> only dump to i18n file if we have a translation





include::{contentdir}/include-source.txt[]
