= Organizando eventos
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2018-02-4
ifndef::backend-pdf[]
:username: jorge-aguilera
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc, google, calendar
:jbake-category: google
:jbake-script: /scripts/google/Sheet2Calendar.groovy
:idprefix:
:imagesdir: ../images
:jbake-english: sheet2calendar-en
endif::[]



[abstract]
Para este post vamos a necesitar los siguientes requisitos: una cuenta, una SpreadSheet y un Calendario todo ello de Google
. Se recomienda crear un calendario específico para este ejercicio puesto que vamos a borrar y crear eventos en el mismo
de forma genérica y si lo hacemos sobre el principal podríamos perder eventos de nuestro interés.
Así mismo para poder acceder a las APIs de Google deberemos obtener unas credenciales que autoricen a la aplicación
para acceder a nuestra cuenta.

:icons: font
NOTE: Este post está inspirado en una iniciativa de Alba Roza (@Alba_Roza) https://twitter.com/Alba_Roza/status/957936830657257473
{set:icons!:}

== Consola Google

En primer lugar deberemos crear una aplicación en la consola de Google en https://console.developers.google.com

En esta aplicación habilitaremos las APIs de "Google Calendar API" y "Google Sheet API"

Así mismo deberemos crear unas credenciales de "Servicio" obteniendo la posibilidad de descargarlas en un fichero JSON
y que deberemos ubicar junto con el script.

== Groogle

Para facilitar la parte técnica de autentificación y creación de servicios he creado un proyecto de código abierto,
*Groogle*, disponible en https://puravida-software.gitlab.io/groogle/ el cual publica un artefacto en Bintray y que
podremos usar en nuestros scripts simplemente incluyendo su repositorio.

Este proyecto se divide a su vez en 4 subprojectos:

- groogle-core, contiene la parte de autentificación principalmente
- groogle-drive, para el manejo de ficheros y carpetas en Drive
- groogle-sheet, para la gestión específica de hojas de cálculo Sheet
- groogle-calendar, para la gestión específica de calendarios

En nuestro caso vamos a utilizar estos dos últimos (el primero se autoincluye por dependencia transitiva)

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=dependencies]
----

Groogle nos permitirá realizar el _login_ de nuestra aplicación y guardar la autorización en nuestro disco de tal forma
que en ejecuciones posteriores no se requiera de autorizar de nuevo la aplicación (mientras no borremos el fichero con
la autorización generada)

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=login]
----

== Identificadores

Inicializamos (por comodidad) el id de la hoja de cálculo así como el del calendario

[source,groovy]
----
sheetId='xxxxxxxxxxxxxxxx_yyyyyyyyy-fMLg'
calendarId='aaaaaaabbbbbbbbbccccccc@group.calendar.google.com'
----

== Sheet

Para nuestro ejemplo la hoja `Eventos` a utilizar será muy sencilla y contendrá la siguiente estructura:

[%header,cols=5*]
|===
|año | mes | día | evento | lugar
|2018 | 1 | 1 | Fiesta Año Nuevo | mi casa
|2018 | 2 | 1 | Codificando como locos | Mordor
|===

Como los eventos que vamos a gestionar son eventos de _todo el día_ vamos a formatear la fecha a YYYY-MM-DD lo cual
nos permitirá posteriormente crear eventos en el calendario de forma muy cómoda. Si por ejemplo quisieramos tratar
eventos "desde-hasta" simplemente modificaríamos la estructura de la hoja y no realizaríamos este formateo

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=readFromSheet]
----

Recorremos todas las filas (excepto la primera) y recogemos en un mapa los detalles de cada evento. Al finalizar tendremos
una lista de eventos que podremos "limpiar", unificar, etc


== Calendar

Una vez que disponemos de los eventos realizaremos en primer lugar una limpieza del calendario:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=removeFromCalendar]
----

Por último simplemente tenemos que recorrer la lista de los eventos e ir creandolos en nuestro calendario. Como ya hemos
dicho nuestro ejemplo es para eventos _todo el día_ por lo que usaremos el método *allDay yyyy-MM-dd* Si quisieramos
gestionar eventos más complejos (varios días, desde una hora hasta otra, etc) utilizaríamos el método *between dateTime1
dateTime2*

Así mismo podríamos añadir recordatorios a ciertos eventos, añadir lista de personas interesadas etc.

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=writeToCalendar]
----

include::{contentdir}/include-source.txt[]
