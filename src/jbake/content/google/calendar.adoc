= A qué dedicas tu tiempo libre (Google Calendar)
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-12-3
ifndef::backend-pdf[]
:username: jorge-aguilera
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc, twitter, javafx, google, calendar
:jbake-category: google
:jbake-script: /scripts/google/Calendar.groovy
:idprefix:
:imagesdir: ../images
:jbake-english: calendar-en
endif::[]



[abstract]
Para este post vamos a necesitar los siguientes requisitos: una cuenta en Google y eventos creados en el calendario.
Por defecto usaremos el calendario principal (pero se podría usar cualquier otro de los que Google nos permite crear).
Así mismo para poder acceder a las APIs de Google deberemos obtener unas credenciales que autoricen a la aplicación
para acceder a nuestra cuenta.

En este post vamos a utilizar un GroovyScript para conectar el calendario de Google revisando todos los
eventos que se encuentran creados, agrupándolos en función del día de la semana y el "tema" que le hayamos asignado.

:icons: font
NOTE: Un evento del calendario de por sí NO incluye ningún campo del tipo "tema" por lo que para poder agruparlos podríamos
usar, por ejemplo, algún identificador en el texto del mismo. En este ejemplo lo que vamos a usar es la capacidad de
asignar un color a un evento, de tal forma que si en nuestro calendario asignamos el color "rojo" a los eventos de
tipo "ocio", el color "azul" a los de "formación", etc. podremos de una forma muy fácil agrupar eventos dispares.
{set:icons!:}

== Consola Google

En primer lugar deberemos crear una aplicación en la consola de Google en https://console.developers.google.com

En esta aplicación habilitaremos (al menos) la API "Google Calendar API"

Así mismo deberemos crear unas credenciales de "Servicio" obteniendo la posibilidad de descargarlas en un fichero JSON
y que deberemos ubicar junto con el script.

== Groogle

Para facilitar la parte técnica de autentificación y creación de servicios he creado un proyecto de código abierto,
*Groogle*, disponible en https://gitlab.com/puravida-software/groogle el cual publica un artefacto en Bintray y que
podremos usar en nuestros scripts simplemente incluyendo su repositorio.

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=dependencies]
----

Groogle nos permitirá realizar el _login_ de nuestra aplicación y guardar la autorización en nuestro disco de tal forma
que en ejecuciones posteriores no se requiera de autorizar de nuevo la aplicación (mientras no borremos el fichero con
la autorización generada)

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=login]
----
<1> *client-secret.json* es el fichero que hemos descargado de la consola de Google y que contiene las credenciales
<2> Groogle realiza el login y guarda la autorización en $HOME/.credentials/${applicationName}
<3> En este post únicamente requerimos acceso a Calendar para lectura

== Personalización

Como los temas son personales cada uno deberá configurar los suyos según sus preferencias. Así mismo creará más o menos
temas según el grado de detalle que se quiera obtener. Para este post vamos a utilizar únicamente 3 temas:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=temas]
----
<1> Hasta ahora no he encontrado cómo saber el ID del color a priori por lo que deberás ejecutarlo primero y ver
cuales te asigna.


== Business

La lógica de negocio de este script consistirá básicamente en recorrer todos los eventos que nos devuelva Google y
para cada uno de ellos buscar en un mapa si existe un sub-mapa "tema" y si para este sub-mapa existe otro sub-map para
el día de la semana donde ir acumulando eventos. En caso de que no exista, simplemente lo inicializaremos a cero para
ir acumulando sobre él.

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=business]
----
<1> Recorrer todos los eventos que nos devuelva Google
<2> Un evento puede ser puntual o recurrente. En este caso deberemos obtener todas las instancias del mismo
<3> Buscar en el mapa el tema asociado al evento y si no existe inicializarlo
<4> Incrementar en uno para el día de la semana del evento

== Vista

Para la vista usaremos un link:http://groovyfx.org/[GroovyFX] que nos muestre una gráfica de barras *barChart* tal que nos permite visualizar
agrupados por temas los diferentes contadores de los días de la semana. Sin embargo este componente nos pide que
le proporcionemos la serie de datos en una sucesión de 'clave', 'valor', 'clave', 'valor' en lugar de un mapa, por lo
que lo primero que haremos será transformar nuestro mapa en uno más adecuado:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=flatten]
----

Y ahora ya podemos construir la vista, generando las series de una forma dinámica.

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=vista]
----
<1> Creamos dinámicamente las series en función de los temas que tengamos inicialmente.


Mi calendario quedaría de esta forma:

image::calendar.png[]

include::{contentdir}/include-source.txt[]
