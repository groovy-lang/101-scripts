var lunrDocuments = [];
var documents={};

<%published_posts.each {post -> %>
lunrDocuments.push({
    title:'${post.title}',
    tags:'${post.tags.join(',')}',
    href:'${post.uri}'
});
documents['${post.uri}']={title:'${post.title}',lang:'${post.lang ?: 'es'}'};
<%}%>

var lunrIdx = lunr(function () {
  this.ref('href');
  this.field('title',{ boost: 10 });
  this.field('tags',{ boost: 10 });
  lunrDocuments.forEach(function (doc){this.add(doc)},this);
});

jQuery(document).ready(function () {
     jQuery('input#search').on('keyup', function () {
         var query = "*"+jQuery(this).val()+"*";
         var result = lunrIdx.search("*"+query+"*");
         var resultdiv = jQuery('ul.searchresults');
         if (query === "**" || result.length === 0) {
            resultdiv.hide();
        } else {
            resultdiv.empty();
            for (var item in result) {
                var ref = result[item].ref;
                var a =  jQuery('<a />',{href:'/101-scripts/'+ref}).appendTo(jQuery('<li />').appendTo(resultdiv));
                jQuery('<span />',{class:'flag-icon flag-icon-'+documents[ref].lang}).appendTo(a);
                jQuery('<span />',{text:documents[ref].title}).appendTo(a);
            }
            resultdiv.show();
        }
    });
});