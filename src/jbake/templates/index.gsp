<%include "header.gsp"%>
<%
    def hay= published_posts.collect{ it.script }.unique().size()
    def titles=[
                basico:[title:'Basico',desc:'Scripts básicos'],
                file:[title:'Ficheros',desc:'Buscar, reemplazar, convertir...'],
                bbdd:[title:'BaseDatos',desc:'Export/Import, batch,...'],
                network:[title:'Red',desc:'Distribuir configuracion, estado de red'],
                docker:[title:'Docker',desc:'Usar Groovy con Docker'],
                office:[title:'Office',desc:'Excel, Twitter, Reports'],
                webservice:[title:'WebService',desc:'Integracion de sistemas SOAP, Rest'],
                javafx:[title:'JavaFX',desc:'Capturar imagen, generar graficos'],
                google:[title:'Google',desc:'Sheet, Drive, Calendar'],
                asciidoctor:[title:'Documentar',desc:'Groovy y Asciidoctor'],
                gradle:[title:'Gradle',desc:'Scripts vs Tasks'],
    ]
%>
	<%include "menu.gsp"%>

    <div class="page-header">
        <h3>Total Post: ${published_posts.size()}, Total Scripts :${hay}</h3>
    </div>

    <div class="container-fluid">
        <div class="row">
            <%titles.eachWithIndex{ category , idx->%>
                <%if(idx%3==0){%> <div class="clearfix visible-xs-block"></div> <%}%>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img src="./images/menu/${category.key}.png" style="height:150px;width:auto;">
                        <div class="caption">
                            <p lang="es">${category.value.desc}</p>
                            <button type="button" class="btn btn-primary" lang="es"
                                    data-toggle="modal"
                                    data-target="#collapse_${category.key}"
                                    aria-expanded="false" aria-controls="collapse_${category.key}">
                                ${category.value.title} <span class="badge badge-light">
                                ${published_posts.findAll{it.category==category.key}.size()}
                            </span>
                            </button>

                        </div>
                    </div>
                    <div class="modal fade" id="collapse_${category.key}" tabindex="-1" role="dialog" aria-labelledby="collapse_${category.key}Label">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="collapse_${category.key}Label">${category.value.title}</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                    <% ['es','gb'].each{ lang->  %>
                                        <div class="col-md-6">
                                            <ul class="list-group">
                                                <%published_posts.findAll({
                                                    (it.lang==null && lang=='es' || it.lang==lang)&&it.category==category.key
                                                }).sort{a,b->a.file<=>b.file}.each {post ->%>
                                                <li class="list-group-item">
                                                    <a href="${post.uri}" class="list-group-item list-group-item-action">
                                                        <span class="flag-icon flag-icon-${lang}">&nbsp;</span>${post.title}
                                                    </a>
                                                </li>
                                                <%}%>
                                            </ul>
                                        </div>
                                    <% } %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <%}%>
        </div>
    </div>

<%include "footer.gsp"%>
