Lang.prototype.pack.gb = {
    "token": {
        "Inicio":"Home"
        ,"Presentaciones":"Slides"
        ,"Acerca":"About"
        ,"Acerca":"About"
        ,'Basico':'Basic'
        ,'Scripts básicos':'Basic scripts'
        ,'Ficheros':'Files'
        ,'Buscar, reemplazar, convertir...':'Search and replace,...'
        ,'BaseDatos':'Database'
        ,'Red':'Network'
        ,'Distribuir configuracion, estado de red':'Configuration, status...'
        ,'Usar Groovy con Docker':'Groovy and Docker'
        ,'Integracion de sistemas SOAP, Rest':'SOAP, Rest'
        ,'Capturar imagen, generar graficos':'Image generation, screenshoots...'
        ,'Documentar':'Documentation'
        ,'Buscar...':'Search...'
    }
}
