//tag::dependencies[]
@Grab(group='com.puravida.groogle', module='groogle-sheet', version='2.0.0')
import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.*
import com.puravida.groogle.sheet.*
import groovy.swing.SwingBuilder

import java.awt.BorderLayout
import java.awt.BorderLayout as BL
import javax.swing.*
//end::dependencies[]

List<List> all = []

GroogleBuilder.build {
//tag::login[]
    credentials {
        applicationName 'raffle'
        withScopes SheetsScopes.SPREADSHEETS
        usingCredentials "client_secret.json"
        asService false
    }
    service(SheetServiceBuilder.build(), SheetService)
//end::login[]
}.service(SheetService.class).withSpreadSheet "1j5JCInCwMfe2eTjvyoA52SGqbAwqF5OR6JFTGuCSweY", {
//tag::load[]
    withSheet 'raffle-example-meetup-1', {
        int i=2
        range = writeRange("A$i", "C${i+99}").get()
        while( range ){
            all.addAll range
            i+=99
            range = writeRange("A$i", "C${i+99}").get()
        }
    }
//end::load[]
}

//tag::swing[]
new SwingBuilder().edt {
    frame(title: 'GroogleRaffe',
            defaultCloseOperation: JFrame.EXIT_ON_CLOSE,
            extendedState: JFrame.MAXIMIZED_BOTH,
            show: true) {

        borderLayout()

        panel(constraints: BorderLayout.PAGE_START){
            label("Meetup Raffle")
        }

        panel(constraints: BorderLayout.PAGE_END){
            button(text:'Raffle',
                    actionPerformed: {
                        Random rnd = new Random()
                        int selected = rnd.nextInt(all.size())
                        String msg = "<html><b>Is <font color='red'>${all[selected][0]} ${all[selected][1]} here ?</font></b></html>"
                        int yepes = JOptionPane.showConfirmDialog(current,msg)
                        if( yepes != JOptionPane.YES_OPTION  ){
                            theList.model.remove(selected)
                        }
                    }, constraints:BL.SOUTH)

        }

        scrollPane(constraints: BorderLayout.CENTER){
            list(id:'theList', listData: all,
                    cellRenderer: { list, value, index, isSelected, cellHasFocus->
                        new JLabel(value[0]+" "+value[1])
                    }
            )
        }

    }
}
//end::swing[]