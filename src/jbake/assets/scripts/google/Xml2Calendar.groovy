//tag::dependencies[]
@Grab(group='io.github.http-builder-ng', module='http-builder-ng-apache', version='1.0.3')
@Grab(group='ch.qos.logback', module='logback-classic', version='1.2.3')

@Grab(group = 'com.puravida.groogle', module = 'groogle-core', version = '1.5.0-alpha2')
@Grab(group = 'com.puravida.groogle', module = 'groogle-calendar', version = '1.5.0-alpha2')
@GrabConfig(systemClassLoader=true)

import com.google.api.services.calendar.CalendarScopes
import com.puravida.groogle.GroogleScript
import com.puravida.groogle.CalendarScript

import groovyx.net.http.*
import static groovyx.net.http.HttpBuilder.configure
import static groovyx.net.http.ContentTypes.JSON
import static groovy.json.JsonOutput.prettyPrint
import static groovy.json.JsonOutput.toJson

//end::dependencies[]

//tag::http[]
XML = ["application/xml", "text/xml", "application/xhtml+xml", "application/atom+xml"]
xmlEncoder = NativeHandlers.Encoders.&xml

http = configure {
    request.uri = 'https://datos.madrid.es/egob/catalogo/206717-0-agenda-eventos-bibliotecas.xml'    
    request.encoder XML, xmlEncoder
    request.contentType = 'application/xml'
}.get{

}
//end::http[]

Date fromDate = new Date() - 10

CalendarScript.instance.with {
    //tag::login[]
    login {
        applicationName 'groogle-example'
        withScopes CalendarScopes.CALENDAR
        usingCredentials new File('client_secret.json')
        asService false
    }
    //end::login[]

    //tag::prepare[]
    String groogleCalendarId = "0v757vib7jf1hj0bjsq041vco8@group.calendar.google.com"
    withCalendar groogleCalendarId, {
        eachEvent{
            removeFromCalendar()
        }
    }
    //end::prepare[]

    //tag::business[]
    http.contenido.each{
        String titulo = it.atributos.atributo.find{ it.'@nombre'=='TITULO-ACTIVIDAD' }.text()
        String descripcion = it.atributos.atributo.find{ it.'@nombre'=='DESCRIPCION' }.text()
        String inicio  = it.atributos.atributo.find{ it.'@nombre'=='FECHA-EVENTO' }.text()
        String fin  = it.atributos.atributo.find{ it.'@nombre'=='FECHA-FIN-EVENTO' }.text()
        String hora  = it.atributos.atributo.find{ it.'@nombre'=='HORA-EVENTO' }.text()
        String where  = it.atributos.atributo.find{ it.'@nombre'=='LOCALIZACION' }?.atributo.find{it.'@nombre'=='NOMBRE-INSTALACION'}.text()

        Date dini = Date.parse('yyyy-MM-dd HH:mm:ss.S', inicio)
        Date dfin = Date.parse('yyyy-MM-dd HH:mm:ss.S', fin)        

        if( descripcion.trim()=="" )
            return
        if( dini < fromDate )
            return

        createEvent groogleCalendarId, {
            it.event.summary = titulo
            it.event.description = "($hora) $descripcion"
            it.event.location=where
            from dini 
            until dfin
        }
    }
    //end::business[]
}