//tag::dependencies[]
@GrabResolver(name='puravida', root="https://dl.bintray.com/puravida-software/repo" )
@Grab(group = 'com.puravida.groogle', module = 'groogle-core', version = '1.4.1')
@Grab(group = 'com.puravida.groogle', module = 'groogle-sheet', version = '1.4.1')
@Grab(group = 'com.puravida.groogle', module = 'groogle-calendar', version = '1.4.1')
@GrabConfig(systemClassLoader=true)

import com.google.api.services.calendar.CalendarScopes
import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.GroogleScript
import com.puravida.groogle.CalendarScript
import com.puravida.groogle.SheetScript

//end::dependencies[]

sheetId='1hf6q540q45gvhdHH9BOVv_Ij7Wk0KVPFCs42Xc-fMLg'
calendarId='puravida-software.com_n9tci5mfaqqdegi5askjetpa7s@group.calendar.google.com'

//tag::login[]
GroogleScript.instance.applicationName='101-groovy'
clientSecret = this.class.getResource('client_secret.json').newInputStream()
GroogleScript.instance.login(clientSecret,[CalendarScopes.CALENDAR, SheetsScopes.SPREADSHEETS])
CalendarScript.instance.groogleScript=GroogleScript.instance
SheetScript.instance.groogleScript=GroogleScript.instance
//end::login[]

//tag::readFromSheet[]
events=[]
SheetScript.instance.withSpreadSheet sheetId, {
    withSheet 'Eventos',{
        int rowIdx=2

        def row = readRows("A$rowIdx","E$rowIdx")
        def calendarScript = CalendarScript.instance

        while( row ){
            def when =sprintf( '%1$04d-%2$02d-%3$02d',row.first()[0] as int,row.first()[1] as int,row.first()[2] as int)
            def summary ="${row.first()[3]}"
            def description="${row.first()[4]}"
            println "$when $summary $description"
            events.add([when:when,summary:summary,description:description])
            rowIdx++
            row = readRows("A$rowIdx","E$rowIdx")
        }
    }
}
//end::readFromSheet[]

//tag::removeFromCalendar[]
CalendarScript.instance.withCalendar( calendarId, {
    eachEvent {
        removeFromCalendar()
    }
})
//end::removeFromCalendar[]


//tag::writeToCalendar[]
events.each{ evt ->
    CalendarScript.instance.createEvent( calendarId,{
        event.summary = evt.summary
        event.description = evt.description
        allDay evt.when
    })
}
//end::writeToCalendar[]
