@GrabConfig(systemClassLoader=true)
@Grab(group='org.xerial', module='sqlite-jdbc', version='3.21.0')

import groovy.sql.Sql

void prepareDatabase() {
    Sql sql = Sql.newInstance("jdbc:sqlite:greach2018.db")
    sql.execute "drop table if exists products"
    String sentence = """CREATE table if not EXISTS products
        (id NUMBER(8), name VARCHAR(40), description VARCHAR(200), price NUMBER(5,2), sales NUMBER(8) )
    """
    sql.execute sentence

    id=0
    sql.execute "insert into products values(${++id}, 'greach2013', 'the most awesome conference in 2013', 50,50)"
    sql.execute "insert into products values(${++id}, 'greach2014', 'the most awesome conference in 2014', 70,75)"
    sql.execute "insert into products values(${++id}, 'greach2015', 'the most awesome conference in 2015', 75,150)"
    sql.execute "insert into products values(${++id}, 'greach2016', 'the most awesome conference in 2016', 80,250)"
    sql.execute "insert into products values(${++id}, 'greach2017', 'the most awesome conference in 2017', 90,300)"
    sql.execute "insert into products values(${++id}, 'greach2018', 'the most awesome conference in 2018', 100,400)"
}

void dumpDatabase() {
    Sql sql = Sql.newInstance("jdbc:sqlite:greach2018.db")
    println '-'.multiply(10)
    sql.rows("select * from products").each{
        println it
    }
    println '-'.multiply(10)
}

prepareDatabase()

dumpDatabase()