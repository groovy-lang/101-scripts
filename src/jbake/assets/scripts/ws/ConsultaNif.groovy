//tag::dependencies[]
@Grab('com.github.groovy-wslite:groovy-wslite:1.1.2')
@Grab('mysql:mysql-connector-java:5.1.6')
@GrabConfig(systemClassLoader=true)
//end::dependencies[]

import groovy.sql.Sql
import wslite.soap.*
import groovy.xml.*

try{
	sql=Sql.newInstance("jdbc:mysql://localhost/mydatabase",args[0], args[1], "com.mysql.jdbc.Driver")


	//tag::cliente[]
	def client = new SOAPClient('https://www1.agenciatributaria.gob.es/wlpl/BURT-JDIT/ws/VNifV2SOAP') //<1>
	//end::cliente[]

	//tag::cabecera[]
	def response = client.send() {
	     envelopeAttributes([
                "xmlns:vnif": "http://www2.agenciatributaria.gob.es/static_files/common/internet/dep/aplicaciones/es/aeat/burt/jdit/ws/VNifV2Ent.xsd" //<1>
		        ])
	//end::cabecera[]
	//tag::body[]
	    body {

		sql.eachRow("select * from nifes where estado is null or estado <> 'IDENTIFICADO' "){ row->	//<1>
			'vnif:VNifV2Ent' {	//<2>
			    'vnif:Contribuyente'{	//<3>
					'vnif:Nif'(row.nif)
					'vnif:Nombre'(row.nombre.toUpperCase())
			    }
			}
		}
	//end::body[]
	    }
	}

	batchSize=20
    erroneos=0

	//tag::respuesta[]
	sql.withBatch( batchSize, "update people set nombre_aeat=?,estado=? where nif=?"){ ps->
		response.VNifV2Sal.Contribuyente.each{ result ->	//<1>
			ps.addBatch([
					"$result.Nombre".toString(),	//<2>
					"$result.Resultado".toString(),
					"$result.Nif".toString()
			])
			erroneos+= "$result.Resultado".equals('IDENTIFICADO') ? 0 : 1
		}
	}
	//end::respuesta[]
	println "Hay $erroneos no identificados o identificados parcialmente"

	//tag::exceptions[]
} catch (SOAPFaultException sfe) { //<1>
    println sfe.request?.contentAsString
    println sfe.request.contentAsString
} catch (SOAPClientException sce) { //<2>
    // This indicates an error with underlying HTTP Client (i.e., 404 Not Found)
    println sce.request?.contentAsString
    println sce.response?.contentAsString    
}
	//end::exceptions[]
