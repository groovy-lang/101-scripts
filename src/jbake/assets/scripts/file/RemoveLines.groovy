origen=new File('dump.txt').text.split('\n') as List

new File('errors.txt').withReader{ reader ->
    line=''
    while( line=reader.readLine() ){
        String id=line.split(' ')[0]    //<1>
        origen.removeIf{ it.startsWith(id) }    //<2>
    }
}

new File('cleaned.txt').withWriter('ISO-8859-1',{
    it.write origen.join('\n')  //<3>
})