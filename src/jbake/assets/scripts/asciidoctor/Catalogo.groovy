
@Grapes([
        @Grab(group='org.asciidoctor', module='asciidoctorj', version='1.5.6'),
        @Grab(group='org.asciidoctor', module='asciidoctorj-pdf', version='1.5.0-alpha.16'),
        @Grab(group='org.jruby', module='jruby-complete', version='9.1.15.0'),
        @Grab('mysql:mysql-connector-java:5.1.6')
])
@GrabConfig(systemClassLoader=true)

import groovy.sql.Sql
import org.asciidoctor.OptionsBuilder
import org.asciidoctor.AttributesBuilder
import org.asciidoctor.SafeMode
import org.asciidoctor.Asciidoctor.Factory
//tag::prepararEntorno[]

template=new File('./product.tpl').text
catalog= new File('./catalog.tpl').text
query='select * from products where sku < 3'

new File('.').eachFileMatch(~/.*.adoc/) { file ->
    file.delete()
}

engine = new groovy.text.SimpleTemplateEngine()

//end::prepararEntorno[]

generateProducts()
generateCatalog()
generatePdf()

//tag::generateProducts[]
void generateProducts(){
    Sql sql = Sql.newInstance( "jdbc:mysql://localhost:3306/origen?jdbcCompliantTruncation=false",
            "user",
            "password",
            "com.mysql.jdbc.Driver")
    sql.eachRow(query){item->
        createProductAdoc item
    }
}
//end::generateProducts[]


//tag::create_adoc[]
def createProductAdoc(item){
    def txt = engine.createTemplate(template).make([
            'description':item.description,
            'sku':item.sku,
            'price':item.price
    ]).toString()
    new File("${item.sku}.adoc").text = txt
}
//end::create_adoc[]


//tag::create_catalog[]
void generateCatalog() {

    def txt = engine.createTemplate(catalog).make([
            'today': new Date().format('dd/MM/yyyy HH:mm')
    ]).toString()
    new File("catalogo.adoc").text = txt

    Sql sql = Sql.newInstance("jdbc:mysql://localhost:3306/origen?jdbcCompliantTruncation=false",
            "user",
            "password",
            "com.mysql.jdbc.Driver")
    sql.eachRow(query) { item ->
        new File("catalogo.adoc") << "include::${item.sku}.adoc[]\n"
    }
}
//end::create_catalog[]

//tag::create_pdf[]
void generatePdf(){
    asciidoctor = Factory.create();
    attributes = AttributesBuilder.attributes(). // <1>
            docType('book').
            tableOfContents(true).
            sectionNumbers(true).
            sourceHighlighter("coderay").
            get()

    options = OptionsBuilder.options(). // <2>
            backend('pdf').
            attributes(attributes).
            safe(SafeMode.UNSAFE).
            get()

    asciidoctor.convertFile(new File("catalogo.adoc"), options) // <3>
}
//end::create_pdf[]
