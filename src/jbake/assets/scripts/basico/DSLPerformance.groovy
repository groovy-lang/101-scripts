
dsls = [

"println new Date()",

"println 1",

"""
println new Random().with {(1..9).collect {(('a'..'z')).join()[ nextInt((('a'..'z')).join().length())]}.join()}
"""

]

database = [:]
dsls.each{
	database[it] = new GroovyShell().parse(it)
}


void executeDSL( int idx ){  
  database[ dsls[idx] ].run()
}

// wait to jconsole
sleep 1000*10

// run a lot of times
(0..100000).each{

  executeDSL( (it % dsls.size()) )

  if( (it % 1000) == 0) { 
	sleep 2000 
	println "liberando "
	System.gc()
	sleep 2000
  }
}
