@Grab('org.yaml:snakeyaml:1.17')
import org.yaml.snakeyaml.Yaml

Yaml parser = new Yaml()

config = parser.load( new File('config_script.yml').text )

println config.doesntExists ?: "doesnExists doesn't exists"

println config.dataSources?.development?.url

println config.dataSources?.production?.url

config.logins.eachWithIndex{ user, idx->
    println "index $idx:"
    println "$user.username = $user.password"
}