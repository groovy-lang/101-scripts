//tag::dependencies[]
@Grab('com.aestasit.infrastructure.sshoogr:sshoogr:0.9.25')
@GrabConfig(systemClassLoader=true)
import static com.aestasit.infrastructure.ssh.DefaultSsh.*
//end::dependencies[]

options.trustUnknownHosts = true

//tag::config[]
def hosts = ['server_1','server_2','server_3','server_4','server_5']
def file = '/var/log/www/access_log'
//end::config[]

//tag::main[]
hosts.each{host->
    getFile(file,host).each{line->
        println line
    }
}
//end::main[]

//tag::read[]
def getFile(dir,host){
  remoteSession("user:passwd@$host:22") {// <1>
        result =  remoteFile(dir).text // <2>
     }
  return result.readLines()// <3>

}
//end::read[]
