import groovy.transform.Memoized

ITERACIONES = 300

memoize=true
counter=0

//tag::funcionesMemoized[]
@Memoized
//tag::funcionesBasicas[]
int closeTo( float a , float b){ //<1>
    float diff = a-b
    if( 0 == diff || Math.abs(diff) < 0.01){
        return 0
    }
    return a < b ? -1 : 1
}
//end::funcionesBasicas[]
//end::funcionesMemoized[]

//tag::funcionesBasicas[]
float calculaValorInversionPlazo(float cuota, float interes, int plazo){
    counter++  //<2>
    cuota/ Math.pow( (1+interes), plazo)
}
//end::funcionesBasicas[]

//tag::funcionesBasicas[]
float calculaValorInversion(float cuota, float interes, int plazos){
    (1..plazos).sum{n->
        calculaValorInversionPlazo(cuota, interes, n)
    }
}
//end::funcionesBasicas[]

//tag::funcionesMemoized[]
@Memoized
float calculaValorInversionPlazoMemoized(float cuota, float interes, int plazo){
    calculaValorInversionPlazo(cuota, interes, plazo)
}
//end::funcionesMemoized[]

//tag::funcionesMemoized[]
@Memoized
float calculaValorInversionMemoized(float cuota, float interes, int plazos){
    calculaValorInversion(cuota, interes, plazos)
}
//end::funcionesMemoized[]

float valorInversion(float cuota, float interes, int plazos){
    memoize ? calculaValorInversionMemoized(cuota,interes,plazos) : calculaValorInversion(cuota,interes,plazos)
}

//tag::funcionesBasicas[]
float calculaTae( float prestamo, float comision, int plazos ){
    boolean forward=true
    float interes = 0.001
    float step = 0.001
    int deep=1

    float cuota = prestamo / plazos
    float objetivo = prestamo - comision

    float lastVac=valorInversion( cuota, interes, plazos) //<3>

    for(int iter=0; iter<ITERACIONES; iter++){

        int close = closeTo(lastVac, objetivo)

        if( close == 0) {   //<4>
            float tae = ( Math.pow(1+interes, 12) -1 )*100
            return tae
        }

        // estamos por encima del objetivo, poco interes,
        if( close > 0 ){
            if( !forward ){
                deep = deep * 10
                forward = true
            }
            interes += (step/deep)
        }

        // estamos por debajo del objetivo, mucho interes
        if( close < 0 ){
            // si hay que retrodecer cuando ibamos para adelante hay que profundizar
            if( forward ){
                deep = deep * 10
                forward = false
            }
            interes -= (step/deep)
        }

        lastVac=valorInversion( cuota, interes, plazos) //<2>
    }
    return 0
}
//end::funcionesBasicas[]

//tag::util[]
void assertTae( float prestamo, float comision, int plazos, float expected){    //<2>
    float tae=calculaTae(prestamo,comision,plazos)
    println "tae encontrada $tae%"
    if( closeTo(tae, expected) != 0 ){
        throw new RuntimeException(
                String.format("TAE de %f eur a %d meses y apertura %f debia ser %f pero dice %f",
                prestamo, plazos, comision, expected, tae))
    }
}
//end::util[]

//tag::examples[]
def runExamples() {
    examples = [
            [1000, 10, 0, 0],
            [1000, 10, 10, 2.21],
            [500, 10, 10, 4.52],
            [500, 20, 10, 9.37],
            [647, 10, 10, 3.47],
            [10000, 100, 24, 0.97],
    ]

    Date start = new Date()
    (0..10000).each {
        examples.each { List item ->
            assertTae(item[0], item[1], item[2], item[3])
        }
    }
    Date end = new Date()
    end-start
}
//end::examples[]

//tag::compare[]
use(groovy.time.TimeCategory) {
    memoize = false
    counter = 0
    time1 = runExamples()
    counter1 = counter

    memoize = true
    counter = 0
    time2= runExamples()
    counter2 = counter

    println "cuando memoize es false, pasamos por counter $counter1 veces. Tardo $time1"
    println "cuando memoize es true, pasamos por counter $counter2 veces. Tardo $time2"
}
//end::compare[]