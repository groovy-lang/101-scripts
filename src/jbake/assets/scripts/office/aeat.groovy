//tag::dependencies[]
@Grab('org.jsoup:jsoup:1.12.1')
@Grab('org.twitter4j:twitter4j-core:4.0.6')

import groovy.util.XmlSlurper
import java.text.SimpleDateFormat
import org.jsoup.Jsoup
import twitter4j.TwitterFactory;
import twitter4j.StatusUpdate
//end::dependencies[]

today = new Date()

//tag::findItems[]
url = 'https://www.agenciatributaria.es/AEAT.internet/Inicio/RSS/Todas_las_Novedades/Todas_las_Novedades.xml'
new XmlSlurper().parse(url).channel.item.findAll {
    def c = Calendar.instance
    c.time = new SimpleDateFormat('E, dd MMM yyyy HH:mm:ss z',Locale.ENGLISH).parse("$it.pubDate")
    c.time - today == -1
}
//end::findItems[]
		.each{
//tag::eachItem[]
	def c = Calendar.instance
	c.time = new SimpleDateFormat('E, dd MMM yyyy HH:mm:ss z',Locale.ENGLISH).parse("$it.pubDate")
	String title=  Jsoup.parse("${c.time.format('dd/MM/yyyy')}\n$it.title").text()
	String body=  Jsoup.parse("${it.description}").text().split(' ').take(50).join(' ')
	String link = "Más info en $it.link"
	String hashtags = "#AEAT"

	def tweets = splitText("$title\n$body", "$link\n$hashtags")
//end::eachItem[]
//tag::eachTweet[]
	long inReply = 0
	tweets.eachWithIndex{ str, i ->
		String page = tweets.size() == 1 ? "" : "${i+1}/${tweets.size()}"
		StatusUpdate status = new StatusUpdate("$str\n$page").inReplyToStatusId(inReply)
		inReply = TwitterFactory.singleton.updateStatus(status).id
		println status.status
	}
//end::eachTweet[]
}


//tag::split[]
def splitText( String text, String suffix ){
	def ret = []
	def words = text.split(' ')
	def current = ''
	words.eachWithIndex{ w, i ->
		if( current.length() > 200 ){
			ret.add current
			current = ''
		}
		current+= "$w "
	}
	current += "\n$suffix"
	ret.add current
	ret
}
//end::split[]