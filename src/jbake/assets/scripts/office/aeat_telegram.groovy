//tag::dependencies[]
@Grab('org.jsoup:jsoup:1.12.1')
@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')

import groovy.util.XmlSlurper
import java.text.SimpleDateFormat
import org.jsoup.Jsoup
import static groovyx.net.http.HttpBuilder.configure
import static groovyx.net.http.ContentTypes.JSON
import groovyx.net.http.*
//end::dependencies[]

//tag::init[]
today = new Date()
properties = new Properties()
new File('telegram.properties').withInputStream {
	properties.load(it)
}
token = properties.getProperty("token")
channel = properties.getProperty("channel")
//end::init[]

//tag::findItems[]
url = 'https://www.agenciatributaria.es/AEAT.internet/Inicio/RSS/Todas_las_Novedades/Todas_las_Novedades.xml'
new XmlSlurper().parse(url).channel.item.findAll {
    def c = Calendar.instance
    c.time = new SimpleDateFormat('E, dd MMM yyyy HH:mm:ss z',Locale.ENGLISH).parse("$it.pubDate")
    c.time - today == -1
}
//end::findItems[]
		.each{
//tag::eachItem[]
	def c = Calendar.instance
	c.time = new SimpleDateFormat('E, dd MMM yyyy HH:mm:ss z',Locale.ENGLISH).parse("$it.pubDate")

	String title=  Jsoup.parse("${it.title}").text()
	String header = "${c.time.format('dd/MM/yyyy')}\n*$title*"
	String body=  Jsoup.parse("${it.description}").text().split(' ').take(50).join(' ')
	String link = "[Más info]($it.link)"

	String message = "$header\n\n$body\n$link"

	configure {
		request.uri = 'https://api.telegram.org'
		request.contentType = JSON[0]
	}.post {
		request.uri.path = "/bot${token}/sendMessage"
		request.uri.query = [ chat_id: channel, text: message, previewMessage:true, parse_mode:'Markdown']
	}
//end::eachItem[]
}