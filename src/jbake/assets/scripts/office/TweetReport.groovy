//tag::dependencies[]
@GrabConfig(systemClassLoader=true)
@Grab(group='org.twitter4j', module='twitter4j-core', version='4.0.6')
@Grab(group='org.xerial', module='sqlite-jdbc', version='3.21.0')
import groovy.sql.Sql
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.StatusUpdate
//end::dependencies[]

//tag::model[]
class TwitterUser{
    String id
    String name
    int tweets
    int followers
    int friends
    String timeZone

    TwitterUser(){
    }

    TwitterUser( String id){
        def tuser = TwitterFactory.singleton.users().showUser(id)
        this.id=id
        this.name = tuser.name
        this.tweets = tuser.statusesCount
        this.followers = tuser.followersCount
        this.friends = tuser.friendsCount
        this.timeZone = tuser.timeZone
    }
}
//end::model[]

//tag::prepare[]
void prepareDatabase(boolean drop) {
    Sql sql = Sql.newInstance("jdbc:sqlite:tweetreport.db")
    if( drop )
        sql.execute "drop table if exists tweetreport"
    String sentence = """CREATE table if not EXISTS tweetreport 
        (id varchar(20), name VARCHAR(40), tweets NUMBER(5), followers NUMBER(5), friends number(5), timezone varchar(10))
    """
    sql.execute sentence
}
//end::prepare[]

//tag::update[]
void updateUser(TwitterUser user){
    Sql sql = Sql.newInstance("jdbc:sqlite:tweetreport.db")
    def row = sql.firstRow("select * from tweetreport where id=?",[user.id])
    String sentence = row ?
            "update tweetreport set name=?.name, tweets=?.tweets, followers=?.followers, friends=?.friends, timezone=?.timeZone where id=?.id"
            :
            "insert into tweetreport (id,name,tweets,followers,friends,timezone) values (?.id,?.name,?.tweets,?.followers,?.friends,?.timeZone)"
    sql.execute sentence, user
}
//end::update[]

List listUsers(){
    Sql sql = Sql.newInstance("jdbc:sqlite:tweetreport.db")
    sql.rows("select id from tweetreport")*.id
}

//tag::report[]
void report(){
    Sql sql = Sql.newInstance("jdbc:sqlite:tweetreport.db")
    sql.eachRow"select * from tweetreport order by id",{ row->
        println row
    }
}
//end::report[]

//tag::cli[]
def cli = new CliBuilder(usage: '-i -u username -r')
cli.with {
    h(longOpt: 'help',    args:0,'Usage Information', required: false)
    i(longOpt: 'initialize',args:0,'Borrar todos los datos y crear la base de datos en limpio', required: false)
    u(longOpt: 'username', args:1, argName:'username', 'El usuario a investigar', required: false)
    a(longOpt: 'all',args:0,'Actualiza la info de todos los usuarios registrados', required: false)
    r(longOpt: 'report',args:0,'Genera un report con los usuarios existentes', required: false)
}
def options = cli.parse(args)
if (options.h  ) {
    cli.usage()
    return
}
//end::cli[]

prepareDatabase(options.i )

if( options.username ) {
    TwitterUser user = new TwitterUser(options.username)
    updateUser(user)
}
if( options.all ){
    listUsers().each{ id->
        println "update $id"
        TwitterUser user = new TwitterUser(id)
        updateUser(user)
    }
}
if( options.report ){
    report()
}
