//tag::dependencias[]

import groovy.transform.EqualsAndHashCode
@GrabConfig(systemClassLoader=true)
@Grab('org.slf4j:slf4j-api:1.7.10')
@Grab('org.grails:grails-datastore-gorm-neo4j:6.1.6.RELEASE')

import groovy.transform.ToString
import grails.gorm.annotation.Entity as EntityAnnotation
import org.grails.datastore.gorm.neo4j.Neo4jDatastore
//end::dependencias[]
@Grab('info.picocli:picocli:2.0.3')

@picocli.groovy.PicocliScript


import groovy.transform.Field
import static picocli.CommandLine.*

//tag::picocli[]
@Option(names= ["-h", "--help"], usageHelp= true, description= "Show this help message and exit.")
@Field boolean helpRequested

@Option(names = ["-b", "--bolt"], description = "url for the bolt driver")
@Field private String url = 'bolt://localhost:7687'

@Option(names = ["-u", "--user"], description = "user to neo4j connection")
@Field private String user = "neo4j"

@Option(names = ["-p", "--password"], description = "password to neo4j connection")
@Field private String password = "password"


@Option(names = ["-a", "--action"], description = ["action to perform as:", "searchOfficer, searchEntity"])
@Field private String action = "searchOfficer"

@Parameters(arity="0", paramLabel="SUBJECT", description="The subject to use.")
@Field String subject
//end::picocli[]

//tag::domain[]
@EntityAnnotation //<1>
@ToString
//tag::plantuml[]
class Officer  {
    String name
    String valid_until
    String icij_id

    //tag::noplantuml[]
    static hasMany = [  //<2>
            director_of : Entity
    ]
    //end::noplantuml[]
}
//end::plantuml[]

@EntityAnnotation
@ToString
//tag::plantuml[]
class Entity {
    String sourceID
    String address
    String former_name
    String jurisdiction
    String struck_off_date
    String service_provider
    String countries
    String jurisdiction_description
    String valid_until
    String ibcRUC
    String original_name
    String name
    String inactivation_date
    String country_codes
    String incorporation_date
    String status
}
//end::plantuml[]

@EntityAnnotation
@ToString
//tag::plantuml[]
class Intermediary {
    String sourceID
    String valid_until
    String name
    String country_codes
    String countries
    String status
}
//end::plantuml[]

@EntityAnnotation
@ToString
//tag::plantuml[]
class Address {
    String address
}
//end::plantuml[]

//end::domain[]

//tag::configuration[]
Map getConfiguration(){
    [
            'grails.neo4j.url': url,
            'grails.neo4j.username': user,
            'grails.neo4j.password': password
    ]
}


Neo4jDatastore datastore
void initDataStore(){
    datastore = new Neo4jDatastore(configuration, Officer, Entity, Intermediary, Address)
}
//end::configuration[]

//tag::complexQuery[]
void searchOfficer(String search) {
    Officer.withTransaction {

        Officer.where {
            name ==~ "%${search}%"
            join 'director_of'      //<1>

        }.list().eachWithIndex { Officer officer, int idx ->
            println "$idx: $officer.name"

            officer.director_of.each{ director_of ->
                println "\t $director_of.name, $director_of.address"
            }
        }
    }
}
//end::complexQuery[]


initDataStore()

//tag::simpleQuery[]
println "We have ${Officer.count()} officers"   //<1>
println "We have ${Entity.count()} entities"
println "We have ${Intermediary.count()} intermediaries"

Officer.withTransaction {   //<2>

    def pilarBorBon = Officer.findByName('Pilar de Borbón') //<3>

    println "We have an interesting Officer $pilarBorBon.name"

    pilarBorBon.director_of.each {  //<4>
        println "\t $it.name"
    }
}
//end::simpleQuery[]

if( subject)
    this."$action"(subject)
